module github.com/jlo87/microservices/hello-world-api

go 1.15

require (
	github.com/mitchellh/mapstructure v1.1.2
	github.com/spf13/viper v1.7.1
	github.com/stretchr/objx v0.3.0 // indirect
	github.com/stretchr/testify v1.3.0
)


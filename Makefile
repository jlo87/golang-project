.PHONY: help all build clean update static static-test-server test test-server test-guidocker-login docker-build docker-run docker-tag docker-push

cnf ?= config.env
include $(cnf)
export $(shell sed 's/=.*//' $(cnf))

dpl ?= deploy.env
include $(dpl)
export $(shell sed 's/=.*//' $(dpl))

## HELP
## This will output the help for each task
help:
	@grep -E '^[a-zA-Z_-]+:.*?## .*$$' $(MAKEFILE_LIST) | sort | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}'

all: build

build: build-server

test: test-server

static: static-test-server

## PROJECT BUILD
build-server:
	go build -ldflags "-X main.buildVersion=$(RPM_VERSION) -X main.gitCommit=$(GIT_COMMIT)" hello.go

## UNIT TESTS
test-server:
	go test ./hello

test-web:
	@echo "NOT YET IMPLEMENTED (polymer test)"

static-test-server:
	go vet ./

## DOCKER BUILD
docker-login:
	 aws ecr get-login-password --region $(AWS_CLI_REGION) | docker login --username AWS --password-stdin $(DOCKER_REPO)

docker-build:
	docker build -t $(APP_NAME):$(TAG_VERSION) .

docker-run:
	docker run -d -t --env-file=./config.env -p=$(PORT):$(PORT) --name="$(APP_NAME)" $(APP_NAME):$(TAG_VERSION)

docker-tag:
	docker tag $(APP_NAME):$(TAG_VERSION) $(DOCKER_REPO)/$(APP_NAME):$(TAG_VERSION)

docker-push:
	docker push $(DOCKER_REPO)/$(APP_NAME):$(TAG_VERSION)

package main

import (
	"fmt"
	"log"
	"net/http"
	"runtime"
	"github.com/mitchellh/mapstructure"
	"github.com/spf13/viper"
)

var (
	buildVersion string
	Config       *Configuration
)

type ServerConfiguration struct {
	Port string `mapstructure:"port"`
}

type Configuration struct {
	Server ServerConfiguration `mapstructure:",squash"`
}

func HelloWorld(rw http.ResponseWriter, r *http.Request) {
	fmt.Fprintf(rw, "Hello, World. I am Build Version: %s\n", buildVersion)
	fmt.Fprintf(rw, "Running on Go, version: %s\n", runtime.Version())

}

func main() {
	var result map[string]interface{}
	var config Configuration
	viper.SetConfigFile("config.yaml")
	if err := viper.ReadInConfig(); err != nil {
		fmt.Printf("Error reading config file, %s", err)
	}

	err := viper.Unmarshal(&result)
	if err != nil {
		fmt.Printf("Unable to decode into map. %v", err)
	}

	decErr := mapstructure.Decode(result, &config)

	if decErr != nil {
		fmt.Println("Error decoding")
	}

	fmt.Printf("Config:%+v\n", config)

	http.HandleFunc("/", HelloWorld)
	fmt.Println("Bind Address:", 9090)
	log.Fatal(http.ListenAndServe(":9090", nil))
}

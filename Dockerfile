FROM centos:7

COPY golang-project-dev-1.0.0.x86_64.rpm . 

RUN yum install -y golang-project-dev-1.0.0.x86_64.rpm

CMD ["/opt/golang-project/bin/hello"]
